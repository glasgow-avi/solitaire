package com.catch22.solitaire.gui;

import javax.swing.*;
import java.awt.*;

public class Console extends JFrame
{
	private final int height = 600, width = 800;
	private static final String title = "Solitaire";
	static final Color background = new Color(70, 150, 70);
	public JPanel house, reserve;
	public GameBoard board;
	int topHeight, reserveWidth, upWidth, downWidth, bottomHeight;

	public Console()
	{
		setLayout(new FlowLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle(this.title);
		setVisible(true);
		setBackground(background);
		setResizable(false);
		setSize(this.width, this.height);

		initPanels();
	}

	private void initPanels()
	{
		reserve = new JPanel();
		house = new JPanel();

		topHeight = getHeight() / 4;
		reserveWidth = getWidth() / 3 - 5;
		upWidth = 2 * getWidth() / 3 - 5;
		downWidth = getWidth() - 5;
		bottomHeight = 3 * getHeight() / 4;

		board = new GameBoard(this);

		reserve.setLayout((new FlowLayout()));
		house.setLayout((new GridLayout(1, 4)));
		reserve.setBackground(background);
		house.setBackground(background);
		reserve.setPreferredSize(new Dimension(reserveWidth, topHeight));
		house.setPreferredSize(new Dimension(upWidth, topHeight));
		reserve.setBounds(0, 0, reserveWidth, topHeight);
		house.setBounds(reserveWidth, 0, upWidth, topHeight);

		add(reserve);
		add(house);
		add(board);

		Thread runnable = new Thread()
		{
			@Override
			public void run()
			{
				while(true)
				{
					for(int i = 0; i < 4; i++)
					{
						board.layout.show(board, String.valueOf(i));
						try
						{
							sleep(1000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		};

		runnable.start();
	}

	public static void main(String[] args)
	{
		new Console();
	}
}
