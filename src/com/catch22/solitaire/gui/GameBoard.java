package com.catch22.solitaire.gui;

import javax.swing.*;
import java.awt.*;

public class GameBoard extends JPanel
{
	private static int width, height;
	public static CardLayout layout;

	public GameBoard(Console console)
	{
		setBackground(Console.background);
		setPreferredSize(new Dimension(console.getWidth() - 5, 3 * console.getHeight() / 4));
		setBounds(0, console.topHeight, console.downWidth, console.bottomHeight);

		layout = new CardLayout();
		setLayout(layout);

		JPanel test1 = new JPanel();
		test1.setPreferredSize(new Dimension(10, 10));
		test1.setBackground(Color.red);
		test1.setBounds(0, 0, 10, 10);
		JPanel test2 = new JPanel();
		test2.setPreferredSize(new Dimension(10, 10));
		test2.setBackground(Color.yellow);
		test2.setBounds(5, 5, 15, 15);
		JPanel test3 = new JPanel();
		test3.setPreferredSize(new Dimension(10, 10));
		test3.setBackground(Color.blue);
		test3.setBounds(10, 10, 20, 20);
		JPanel test4 = new JPanel();
		test4.setPreferredSize(new Dimension(10, 10));
		test4.setBackground(Color.green);
		test4.setBounds(15, 15, 25, 25);

		add(test1, "0");
		add(test2, "1");
		add(test3, "2");
		add(test4, "3");
	}
}
